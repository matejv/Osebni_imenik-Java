/**
 * Razširja razred Oseba -> Prijatelj - nova oseba Sorodnik z več atributi.
 */
public class Sorodnik extends Prijatelj {
    private String sorodstvenaVez;

    public Sorodnik(String ime, String priimek, String telSt, String eposta,
                    String facebook, String twitter, String googlePlus, String rojstniDan, String prebivalisce,
                    String sorodstvenaVez) {
        super(ime, priimek, telSt, eposta,
                facebook, twitter, googlePlus, rojstniDan, prebivalisce);
        this.sorodstvenaVez = sorodstvenaVez;
    }

    public String podrobnosti() {
        String izpis = super.podrobnosti();
        izpis += "\nSorodstvena vez: " + this.sorodstvenaVez;

        return izpis;
    }

    // preveri, če katerikoli atribut objetka vsebuje podniz iskalne poizvedbe
    public boolean toSemJaz(String poizvedba) {
        if (super.toSemJaz(poizvedba)) // če poizvedba uspe že med atributi osebe Prijatelj, vrni true
            return true;
        else { // sicer preveri še med atributi osebe Sorodnik
            if (this.sorodstvenaVez.toLowerCase().matches("(.*)" + poizvedba.toLowerCase() + "(.*)"))
                return true;
        }

        // če poizvedba ne uspe nikjer, vrni false
        return false;
    }

    public String shranjevalniIzpis() {
        String izpis = super.shranjevalniIzpis() + ";";

        if (this.sorodstvenaVez.length() > 0)
            izpis += this.sorodstvenaVez;
        else
            izpis += "/";

        izpis = izpis.substring(2);

        return "2;" + izpis;
    }

    public String getSorodstvenaVez() {
        return sorodstvenaVez;
    }

    public void setSorodstvenaVez(String sorodstvenaVez) {
        this.sorodstvenaVez = sorodstvenaVez;
    }
}
