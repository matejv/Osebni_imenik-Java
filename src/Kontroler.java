import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * Povezuje uporabniški vmesnik s kodo.
 */
public class Kontroler implements Initializable {
    // ZAVIHEK Osebni imenik
    @FXML private TextField tfIskanje;
    @FXML private Button btIsci;
    @FXML private Button btPrikaziVse;

    @FXML private TableView<Oseba> tvImenik;
    @FXML private TableColumn tcIme;
    @FXML private TableColumn tcPriimek;
    @FXML private TableColumn tcTelSt;
    @FXML private TableColumn tcEposta;

    @FXML private Button btUredi;
    @FXML private Button btIzbrisi;

    // ZAVIHEK Dodaj osebo
    @FXML private RadioButton rbZnanec;
    @FXML private RadioButton rbPrijatelj;
    @FXML private RadioButton rbSorodnik;
    @FXML private Button btPotrdi;
    // Znanec
    @FXML private TextField tfIme;
    @FXML private TextField tfPriimek;
    @FXML private TextField tfTelSt;
    @FXML private TextField tfEposta;
    // Prijatelj
    @FXML private TextField tfFacebook;
    @FXML private TextField tfTwitter;
    @FXML private TextField tfGooglePlus;
    @FXML private TextField tfRojstniDan;
    @FXML private TextField tfPrebivalisce;
    // Sorodnik
    @FXML private TextField tfSorodstvenaVez;

    @FXML private Button btDodaj;

    private String datoteka = "imenik.txt";
    private int vrstaOsebe = 1; // 1 - Znanec, 2 - Prijatelj, 3 - Sorodnik
    private boolean prikazujemIskanje = false; // prikaz celotnega imenika ali rezultatov iskanja
    Imenik imenik = new Imenik();

    @Override
    public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
        // ZAVIHEK Osebni imenik
        imenik.preberiOsebe(datoteka);
        tcIme.setCellValueFactory(new PropertyValueFactory<Oseba, String>("ime"));
        tcPriimek.setCellValueFactory(new PropertyValueFactory<Oseba, String>("priimek"));
        tcTelSt.setCellValueFactory(new PropertyValueFactory<Oseba, String>("telSt"));
        tcEposta.setCellValueFactory(new PropertyValueFactory<Oseba, String>("eposta"));

        if (! prikazujemIskanje)
            tvImenik.setItems(imenik.getImenikOseb());
        else
            tvImenik.setItems(imenik.getRezultatiIskanja());

        btIsci.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String poizvedba = tfIskanje.getText();
                imenik.isci(poizvedba);
                tvImenik.setItems(imenik.getRezultatiIskanja());
                prikazujemIskanje = true;
            }
        });

        btPrikaziVse.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                tfIskanje.setText("");
                tvImenik.setItems(imenik.getImenikOseb());
                prikazujemIskanje = false;
            }
        });

        btUredi.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (! tvImenik.getSelectionModel().isEmpty()) {
                    Oseba izbrana = tvImenik.getSelectionModel().getSelectedItem();

                    // ustvarjanje novega objekta kontrolerja s prirejenim konstruktorjem za pojavno okno Urejanje osebe
                    KontrolerUredi urediOsebo = new KontrolerUredi(izbrana);

                    FXMLLoader okno = new FXMLLoader(getClass().getResource("guiUredi.fxml"));
                    okno.setController(urediOsebo);

                    Parent oknoUredi = null;
                    try {
                        oknoUredi = okno.load();
                        Stage guiUredi = new Stage();
                        guiUredi.setTitle("Urejanje osebe: " + izbrana.getIme());
                        guiUredi.setResizable(false);
                        guiUredi.setFullScreen(false);
                        guiUredi.setScene(new Scene(oknoUredi, 600, 720));
                        guiUredi.showAndWait(); // počaka, dokler se okno ne zapre in šele nato upošteva spremembe
                    } catch (IOException e) {
                        System.out.println("NAPAKA: vhod/izhod uporabniškega vmesnika");
                        e.printStackTrace();
                    }

                    // osveževanje tabele, da so prikazane najnovejše vrednosti
                    tvImenik.getColumns().get(0).setVisible(false);
                    tvImenik.getColumns().get(0).setVisible(true);

                    imenik.urediOsebo(datoteka);
                    tvImenik.getSelectionModel().clearSelection();
                }
            }
        });

        btIzbrisi.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (! tvImenik.getSelectionModel().isEmpty()) {
                    Oseba izbrana = tvImenik.getSelectionModel().getSelectedItem();

                    imenik.izbrisiOsebo(datoteka, izbrana, prikazujemIskanje);
                    tvImenik.getSelectionModel().clearSelection();
                }
            }
        });

        // ZAVIHEK Dodaj osebo
        rbZnanec.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                vrstaOsebe = 1;
            }
        });

        rbPrijatelj.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                vrstaOsebe = 2;
            }
        });

        rbSorodnik.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                vrstaOsebe = 3;
            }
        });

        btPotrdi.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                rbZnanec.setDisable(true);
                rbPrijatelj.setDisable(true);
                rbSorodnik.setDisable(true);
                btPotrdi.setDisable(true);
                btDodaj.setDisable(false);

                switch (vrstaOsebe) {
                    case 3: {
                        tfSorodstvenaVez.setDisable(false);
                    }
                    case 2: {
                        tfFacebook.setDisable(false);
                        tfTwitter.setDisable(false);
                        tfGooglePlus.setDisable(false);
                        tfRojstniDan.setDisable(false);
                        tfPrebivalisce.setDisable(false);
                    }
                    case 1: {
                        tfIme.setDisable(false);
                        tfPriimek.setDisable(false);
                        tfTelSt.setDisable(false);
                        tfEposta.setDisable(false);
                    }
                }
            }
        });

        btDodaj.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String[] vnosi = new String[10];
                vnosi[0] = tfIme.getText();
                vnosi[1] = tfPriimek.getText();
                vnosi[2] = tfTelSt.getText();
                vnosi[3] = tfEposta.getText();
                vnosi[4] = tfFacebook.getText();
                vnosi[5] = tfTwitter.getText();
                vnosi[6] = tfGooglePlus.getText();
                vnosi[7] = tfRojstniDan.getText();
                vnosi[8] = tfPrebivalisce.getText();
                vnosi[9] = tfSorodstvenaVez.getText();

                if (Imenik.preveriVnos(vnosi)) {
                    switch (vrstaOsebe) {
                        case 1: {
                            Oseba nekdo = new Oseba(vnosi[0], vnosi[1], vnosi[2], vnosi[3]);
                            imenik.dodajOsebo(datoteka, nekdo);
                            uspesenVnos();
                        }
                        break;
                        case 2: {
                            Oseba nekdo = new Prijatelj(vnosi[0], vnosi[1], vnosi[2], vnosi[3],
                                    vnosi[4], vnosi[5], vnosi[6], vnosi[7], vnosi[8]);
                            imenik.dodajOsebo(datoteka, nekdo);
                            uspesenVnos();
                        }
                        break;
                        case 3: {
                            Oseba nekdo = new Sorodnik(vnosi[0], vnosi[1], vnosi[2], vnosi[3],
                                    vnosi[4], vnosi[5], vnosi[6], vnosi[7], vnosi[8],
                                    vnosi[9]);
                            imenik.dodajOsebo(datoteka, nekdo);
                            uspesenVnos();
                        }
                        break;
                    }

                    ponastaviVnos();
                }
                else {
                    Alert napaka = new Alert(Alert.AlertType.ERROR);
                    napaka.getDialogPane().setPrefSize(700, 400);
                    napaka.setTitle("Napaka pri vnosu");
                    napaka.setHeaderText("Neveljaven vnos");
                    napaka.setContentText("Preveri:\n- ali je izpolnjeno vsaj eno polje,\n" +
                            "- ali telefonska številka vsebuje samo števke od 0 do 9 in\n" +
                            "- ali je vnesen elektronski naslov oblike oseba@domena");
                    napaka.showAndWait();
                }
            }
        });

        tvImenik.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                // dvoklik in izbira neprazne vrstice
                if (event.getClickCount() == 2 && (! tvImenik.getSelectionModel().isEmpty())) {
                    Oseba kliknjena = tvImenik.getSelectionModel().getSelectedItem(); // izbran objekt iz tabele
                    prikaziPodrobnosti(kliknjena);
                    tvImenik.getSelectionModel().clearSelection(); // odstrani izbiro vrstice
                }
            }
        });
    }

    private void ponastaviVnos() {
        tfIme.setText("");
        tfPriimek.setText("");
        tfTelSt.setText("");
        tfEposta.setText("");
        tfFacebook.setText("");
        tfTwitter.setText("");
        tfGooglePlus.setText("");
        tfRojstniDan.setText("");
        tfPrebivalisce.setText("");
        tfSorodstvenaVez.setText("");

        // omogočanje gumbov za izbiro vrste osebe
        rbZnanec.setDisable(false);
        rbPrijatelj.setDisable(false);
        rbSorodnik.setDisable(false);
        btPotrdi.setDisable(false);
        // onemogočanje polj za vnos podatkov
        btDodaj.setDisable(true);
        tfIme.setDisable(true);
        tfPriimek.setDisable(true);
        tfTelSt.setDisable(true);
        tfEposta.setDisable(true);
        tfFacebook.setDisable(true);
        tfTwitter.setDisable(true);
        tfGooglePlus.setDisable(true);
        tfRojstniDan.setDisable(true);
        tfPrebivalisce.setDisable(true);
        tfSorodstvenaVez.setDisable(true);
    }

    private void uspesenVnos() {
        Alert obvestilo = new Alert(Alert.AlertType.INFORMATION);
        obvestilo.setTitle("Uspešno dodajanje");
        obvestilo.setHeaderText("Nova oseba je uspešno dodana");
        obvestilo.show();
    }

    private void prikaziPodrobnosti(Oseba izbrana) {
        Alert obvestilo = new Alert(Alert.AlertType.INFORMATION);
        obvestilo.setResizable(true);
        obvestilo.getDialogPane().setPrefSize(550, 600);
        obvestilo.setTitle(izbrana.getIme() + " " + izbrana.getPriimek());

        // nastavitev glave okna informacij glede na vrsto osebe
        if (izbrana instanceof Sorodnik)
            obvestilo.setHeaderText("Podrobnosti sorodnika");
        else if (izbrana instanceof Prijatelj)
            obvestilo.setHeaderText("Podrobnosti prijatelja");
        else
            obvestilo.setHeaderText("Podrobnosti znanca");

        obvestilo.setContentText(izbrana.podrobnosti());
        obvestilo.showAndWait();
    }
}
