import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.*;
import java.util.Scanner;

/**
 * Upravlja z datoteko imenik in seznami oseb imenika.
 */
public class Imenik {
    // seznam, ki omogoča, da se spremembe obravnavajo v realnem času
    // vse osebe iz imenika
    private ObservableList<Oseba> imenikOseb = FXCollections.observableArrayList();
    // rezultati iskanja
    private ObservableList<Oseba> rezultatiIskanja = FXCollections.observableArrayList();

    public void preberiOsebe (String datoteka) {
        Scanner dat = null;
        try {
            dat = new Scanner(new File(datoteka), "UTF-8");

            while (dat.hasNext()) {
                String[] oseba = (dat.nextLine()).split(";");

                switch(oseba[0]) {
                    case "0": {
                        Oseba nekdo = new Oseba(oseba[1], oseba[2], oseba[3], oseba[4]);
                        imenikOseb.add(nekdo);
                    } break;
                    case "1": {
                        Oseba nekdo = new Prijatelj(oseba[1], oseba[2], oseba[3], oseba[4],
                                oseba[5], oseba[6], oseba[7], oseba[8], oseba[9]);
                        imenikOseb.add(nekdo);
                    } break;
                    case "2": {
                        Oseba nekdo = new Sorodnik(oseba[1], oseba[2], oseba[3], oseba[4],
                                oseba[5], oseba[6], oseba[7], oseba[8], oseba[9],
                                oseba[10]);
                        imenikOseb.add(nekdo);
                    } break;
                }
            }

            dat.close();
        } catch (FileNotFoundException e) {
            /* Če podana datoteka ne obstaja, se program normalno zažene, v konzolo izpiše napako in je kljub temu
            normalno funkcionalen. Ob dodajanju nove osebe ustvari podano datoteko. */
            System.out.println("NAPAKA: ne najdem datoteke");
            e.printStackTrace();
        }
    }

    public void isci(String poizvedba) {
        rezultatiIskanja.clear();

        for (Oseba trenutna : imenikOseb) {
            if (trenutna.toSemJaz(poizvedba))
                rezultatiIskanja.add(trenutna);
        }
    }

    public void dodajOsebo(String datoteka, Oseba nekdo) {
        imenikOseb.add(nekdo);

        shraniVDatoteko(datoteka);
    }

    public void urediOsebo(String datoteka) {
        shraniVDatoteko(datoteka);
    }

    public void izbrisiOsebo(String datoteka, Oseba nekdo, boolean prikazujemIskanje) {
        imenikOseb.remove(nekdo);

        // pri prikazu rezultatov iskanja, je pri brisanju osebo treba zbrisati tudi iz rezultatov iskanja
        if (prikazujemIskanje)
            rezultatiIskanja.remove(nekdo);

        shraniVDatoteko(datoteka);
    }

    private void shraniVDatoteko(String datoteka) {
        try {
            PrintWriter izhod = new PrintWriter(new File(datoteka));

            for (Oseba trenutna : imenikOseb) {
                izhod.println(trenutna.shranjevalniIzpis());
            }

            izhod.close();
        } catch (FileNotFoundException e) {
            System.out.println("NAPAKA: pisanje v datoteko");
            e.printStackTrace();
        }
    }

    private static boolean izpolnjenoPolje(String[] vnosi) {
        // preveri, če je izpolnjeno vsaj eno polje
        for (int i = 0; i < vnosi.length; i++)
            if (vnosi[i].length() > 0)
                return true;

        return false;
    }

    private static boolean preveriTelSt(String telSt) {
        // ali so vsi znaki števke od 0 do 9
        for (int i = 0; i < telSt.length(); i++) {
            if (! (telSt.charAt(i) >= '0' && telSt.charAt(i) <= '9'))
                return false;
        }
        return true;
    }

    private static boolean preveriEposto(String eposta) {
        // če vsebuje znak @ in @ ni prvi ali zadnji znak e-poštnega naslova
        if (eposta.matches("(.*)" + "@" + "(.*)") && eposta.charAt(0) != '@' && eposta.charAt(eposta.length()-1) != '@')
            return true;

        return false;
    }

    public static boolean preveriVnos(String[] vnosi) {
        if (! izpolnjenoPolje(vnosi))
            return false;

        // preveri ustreznost telefonske številke, če je vnesena
        if (vnosi[2].length() > 0)
            if (! preveriTelSt(vnosi[2]))
                return false;

        // preveri ustreznost e-poštnega naslova, če je vnesen
        if (vnosi[3].length() > 0)
            if (! preveriEposto(vnosi[3]))
                return false;

        return true;
    }

    public ObservableList<Oseba> getImenikOseb() {
        return imenikOseb;
    }

    public ObservableList<Oseba> getRezultatiIskanja() {
        return rezultatiIskanja;
    }
}
