/**
 * Upravlja z objekti (osebami) imenika.
 */
public class Oseba {
    private String ime;
    private String priimek;
    private String telSt;
    private String eposta;

    public Oseba(String ime, String priimek, String telSt, String eposta) {
        this.ime = ime;
        this.priimek = priimek;
        this.telSt = telSt;
        this.eposta = eposta;
    }

    public String podrobnosti() {
        String izpis = "Ime: " + this.ime + "\n";
        izpis += "Priimek: " + this.priimek + "\n";
        izpis += "Telefonska številka: " + this.telSt + "\n";
        izpis += "E-pošta: " + this.eposta;

        return izpis;
    }

    // preveri, če katerikoli atribut objetka vsebuje podniz iskalne poizvedbe
    public boolean toSemJaz(String poizvedba) {
        if (this.ime.toLowerCase().matches("(.*)" + poizvedba.toLowerCase() + "(.*)"))
            return true;
        else if (this.priimek.toLowerCase().matches("(.*)" + poizvedba.toLowerCase() + "(.*)"))
            return true;
        else if (this.telSt.toLowerCase().matches("(.*)" + poizvedba.toLowerCase() + "(.*)"))
            return true;
        else if (this.eposta.toLowerCase().matches("(.*)" + poizvedba.toLowerCase() + "(.*)"))
            return true;
        return false;
    }

    public String shranjevalniIzpis() {
        String izpis = "0;";

        if (this.ime.length() > 0)
            izpis += this.ime + ";";
        else
            izpis += "/;";

        if(this.priimek.length() > 0)
            izpis += this.priimek + ";";
        else
            izpis += "/;";

        if (this.telSt.length() > 0)
            izpis += this.telSt + ";";
        else
            izpis += "/;";

        if (this.eposta.length() > 0)
            izpis += this.eposta;
        else
            izpis += "/";

        return izpis;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPriimek() {
        return priimek;
    }

    public void setPriimek(String priimek) {
        this.priimek = priimek;
    }

    public String getTelSt() {
        return telSt;
    }

    public void setTelSt(String telSt) {
        this.telSt = telSt;
    }

    public String getEposta() {
        return eposta;
    }

    public void setEposta(String eposta) {
        this.eposta = eposta;
    }
}
