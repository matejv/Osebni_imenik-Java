import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Glavni razred z main metodo, ki prikaže uporabniški vmesnik.
 */
public class OsebniImenik extends Application {
    @Override
    public void start(Stage gui) throws Exception {
        Parent glavnoOkno = FXMLLoader.load(getClass().getResource("gui.fxml"));
        gui.setTitle("Osebni imenik");
        gui.setResizable(false);
        gui.setFullScreen(false);
        gui.setScene(new Scene(glavnoOkno, 1070, 1090));
        gui.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
