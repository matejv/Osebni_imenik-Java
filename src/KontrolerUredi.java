import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

/**
 * Povezuje uporabniški vmesnik okna Uredi s kodo.
 */
public class KontrolerUredi implements Initializable {
    @FXML private TextField tfImeU;
    @FXML private TextField tfPriimekU;
    @FXML private TextField tfTelStU;
    @FXML private TextField tfEpostaU;
    @FXML private TextField tfFacebookU;
    @FXML private TextField tfTwitterU;
    @FXML private TextField tfGooglePlusU;
    @FXML private TextField tfRojstniDanU;
    @FXML private TextField tfPrebivalisceU;
    @FXML private TextField tfSorodstvenaVezU;

    @FXML private Button btShraniSpremembeU;

    Oseba nekdo;

    public KontrolerUredi(Oseba nekdo) {
        this.nekdo = nekdo;
    }

    @Override
    public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
        tfImeU.setText(nekdo.getIme());
        tfPriimekU.setText(nekdo.getPriimek());
        tfTelStU.setText(nekdo.getTelSt());
        tfEpostaU.setText(nekdo.getEposta());

        if(nekdo instanceof Prijatelj) {
            // omogoči polja za vnos atributov Prijatelja
            tfFacebookU.setDisable(false);
            tfTwitterU.setDisable(false);
            tfGooglePlusU.setDisable(false);
            tfRojstniDanU.setDisable(false);
            tfPrebivalisceU.setDisable(false);

            // nastavi trenutne vrednosti atributov v vnosna polja
            tfFacebookU.setText(((Prijatelj) nekdo).getFacebook());
            tfTwitterU.setText(((Prijatelj) nekdo).getTwitter());
            tfGooglePlusU.setText(((Prijatelj) nekdo).getGooglePlus());
            tfRojstniDanU.setText(((Prijatelj) nekdo).getRojstniDan());
            tfPrebivalisceU.setText(((Prijatelj) nekdo).getPrebivalisce());
        }

        if (nekdo instanceof Sorodnik) {
            // omogoči polje za vnos atributa Sorodnika
            tfSorodstvenaVezU.setDisable(false);

            // nastavi trenutno vrednost atributa v vnosno polje
            tfSorodstvenaVezU.setText(((Sorodnik) nekdo).getSorodstvenaVez());
        }

        btShraniSpremembeU.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String[] vnosi = new String[10];
                vnosi[0] = tfImeU.getText();
                vnosi[1] = tfPriimekU.getText();
                vnosi[2] = tfTelStU.getText();
                vnosi[3] = tfEpostaU.getText();
                vnosi[4] = tfFacebookU.getText();
                vnosi[5] = tfTwitterU.getText();
                vnosi[6] = tfGooglePlusU.getText();
                vnosi[7] = tfRojstniDanU.getText();
                vnosi[8] = tfPrebivalisceU.getText();
                vnosi[9] = tfSorodstvenaVezU.getText();

                if (Imenik.preveriVnos(vnosi)) {
                    nekdo.setIme(vnosi[0]);
                    nekdo.setPriimek(vnosi[1]);
                    nekdo.setTelSt(vnosi[2]);
                    nekdo.setEposta(vnosi[3]);

                    if (nekdo instanceof Prijatelj) {
                        ((Prijatelj) nekdo).setFacebook(vnosi[4]);
                        ((Prijatelj) nekdo).setTwitter(vnosi[5]);
                        ((Prijatelj) nekdo).setGooglePlus(vnosi[6]);
                        ((Prijatelj) nekdo).setRojstniDan(vnosi[7]);
                        ((Prijatelj) nekdo).setPrebivalisce(vnosi[8]);
                    }

                    if (nekdo instanceof Sorodnik) {
                        ((Sorodnik) nekdo).setSorodstvenaVez(vnosi[9]);
                    }
                }
                else {
                    Alert napaka = new Alert(Alert.AlertType.ERROR);
                    napaka.getDialogPane().setPrefSize(700, 400);
                    napaka.setTitle("Napaka pri vnosu");
                    napaka.setHeaderText("Neveljaven vnos");
                    napaka.setContentText("Preveri:\n- ali je izpolnjeno vsaj eno polje,\n" +
                            "- ali telefonska številka vsebuje samo števke od 0 do 9 in\n" +
                            "- ali je vnesen elektronski naslov oblike oseba@domena");
                    napaka.showAndWait();
                }
            }
        });
    }
}
