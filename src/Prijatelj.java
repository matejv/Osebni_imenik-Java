/**
 * Razširja razred Oseba - nova oseba Prijatelj z več atributi.
 */
public class Prijatelj extends Oseba {
    private String facebook;
    private String twitter;
    private String googlePlus;
    private String rojstniDan;
    private String prebivalisce;

    public Prijatelj(String ime, String priimek, String telSt, String eposta,
                     String facebook, String twitter, String googlePlus, String rojstniDan, String prebivalisce) {
        super(ime, priimek, telSt, eposta);
        this.facebook = facebook;
        this.twitter = twitter;
        this.googlePlus = googlePlus;
        this.rojstniDan = rojstniDan;
        this.prebivalisce = prebivalisce;
    }

    public String podrobnosti() {
        String izpis = super.podrobnosti();
        izpis += "\nFacebook: " + this.facebook + "\n";
        izpis += "Twitter: " + this.twitter + "\n";
        izpis += "Google+: " + this.googlePlus + "\n";
        izpis += "Rojstni dan: " + this.rojstniDan + "\n";
        izpis += "Prebivališče: " + this.prebivalisce;

        return izpis;
    }

    // preveri, če katerikoli atribut objetka vsebuje podniz iskalne poizvedbe
    public boolean toSemJaz(String poizvedba) {
        if (super.toSemJaz(poizvedba)) // če poizvedba uspe že med atributi osebe Znanec, vrni true
            return true;
        else { // sicer preveri še med atributi osebe Prijatelj
            if (this.facebook.toLowerCase().matches("(.*)" + poizvedba.toLowerCase() + "(.*)"))
                return true;
            else if (this.twitter.toLowerCase().matches("(.*)" + poizvedba.toLowerCase() + "(.*)"))
                return true;
            else if (this.googlePlus.toLowerCase().matches("(.*)" + poizvedba.toLowerCase() + "(.*)"))
                return true;
            else if (this.rojstniDan.toLowerCase().matches("(.*)" + poizvedba.toLowerCase() + "(.*)"))
                return true;
            else if (this.prebivalisce.toLowerCase().matches("(.*)" + poizvedba.toLowerCase() + "(.*)"))
                return true;
        }

        // če poizvedba ne uspe nikjer, vrni false
        return false;
    }

    public String shranjevalniIzpis() {
        String izpis = super.shranjevalniIzpis() + ";";

        if (this.facebook.length() > 0)
            izpis += this.facebook + ";";
        else
            izpis += "/;";

        if (this.twitter.length() > 0)
            izpis += this.twitter + ";";
        else
            izpis += "/;";

        if (this.googlePlus.length() > 0)
            izpis += this.googlePlus + ";";
        else
            izpis += "/;";

        if (this.rojstniDan.length() > 0)
            izpis += this.rojstniDan + ";";
        else
            izpis += "/;";

        if (this.prebivalisce.length() > 0)
            izpis += this.prebivalisce;
        else
            izpis += "/";

        izpis = izpis.substring(2);

        return "1;" + izpis;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getGooglePlus() {
        return googlePlus;
    }

    public void setGooglePlus(String googlePlus) {
        this.googlePlus = googlePlus;
    }

    public String getRojstniDan() {
        return rojstniDan;
    }

    public void setRojstniDan(String rojstniDan) {
        this.rojstniDan = rojstniDan;
    }

    public String getPrebivalisce() {
        return prebivalisce;
    }

    public void setPrebivalisce(String prebivalisce) {
        this.prebivalisce = prebivalisce;
    }
}
